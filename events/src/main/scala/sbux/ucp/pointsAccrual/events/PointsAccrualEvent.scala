package sbux.ucp.pointsAccrual.events

import java.util.UUID

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto._

sealed trait PointsAccrualEvent

object PointsAccrualEvent {

  case class PointsAccrued (memberId: UUID, programId: UUID, modifiedData: Long, initiator: String) extends PointsAccrualEvent
  object PointsAccrued {
    implicit val decodePointsAccrued: Decoder[PointsAccrued] = deriveDecoder[PointsAccrued]
    implicit val encodePointsAccrued: Encoder[PointsAccrued] = deriveEncoder[PointsAccrued]
  }
}
