package sbux.ucp.points.accrual.Database

import java.time.OffsetDateTime
import java.util.UUID

/**
  * this is a case class of the scala representation of the table
  */
case class PointRecord(id: UUID, modifiedDate: OffsetDateTime, point: Int, source: String) {}
