package sbux.ucp.points.accrual.config

import java.net.InetAddress
import com.outworkers.phantom.connectors.{ContactPoint, ContactPoints}
import sbux.ucp.points.common.Config
import scala.collection.JavaConversions._

object DatabaseConfig extends Config {
  protected val databaseConfig = config.getConfig("database")
  val hosts = databaseConfig.getStringList("cassandra.host")
  val inets = hosts.map(InetAddress.getByName)

  val keyspace: String = databaseConfig.getString("cassandra.keyspace")
  lazy val connector = ContactPoints(hosts).withClusterBuilder(
    _.withCredentials(
      databaseConfig.getString("cassandra.username"),
      databaseConfig.getString("cassandra.password")
    )
  ).keySpace(keyspace)

  /**
    * Create an embedded connector, used for testing purposes
    */
  lazy val testConnector = ContactPoint.embedded.noHeartbeat().keySpace("customer_test_db")
}
