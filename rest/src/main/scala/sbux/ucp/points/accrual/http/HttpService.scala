package sbux.ucp.points.accrual.http

import akka.http.scaladsl.server.Directives._
import sbux.ucp.points.accrual.routes.PointsAccrualServiceRoute
import sbux.ucp.points.accrual.services.PointsAccrualService

trait HttpServiceComponent {
  def pointService: PointsAccrualService
}

trait HttpService extends HttpServiceComponent with CorsSupport {
  val pointRoute = new PointsAccrualServiceRoute(pointService)

  val routes =
    pathPrefix("v1") {
      corsHandler(
        pointRoute.route
      )
    }
}
