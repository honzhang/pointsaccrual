package sbux.ucp.points.accrual

import akka.actor.ActorSystem
import akka.event.{Logging, LoggingAdapter}
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import sbux.ucp.points.accrual.http.HttpService
import sbux.ucp.points.accrual.services.{PointsAccrualService, PointsAccrualServiceImpl}
import sbux.ucp.points.common.Config

import scala.concurrent.ExecutionContext
import scala.io.StdIn

/**
  * Project: Points Accrual
  * Author: honzhang on 11/20/2016.
  */
object serviceLauncher extends App with Config{
  implicit val actorSystem = ActorSystem()
  implicit val executor: ExecutionContext = actorSystem.dispatcher
  implicit val log: LoggingAdapter = Logging(actorSystem, getClass)
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  // create a customer repository using a production database
  val httpService = new HttpService {
    override def pointService = new PointsAccrualServiceImpl()
  }

  val bindingFuture = Http().bindAndHandle(httpService.routes, httpHost, httpPort)
  println(s"server is starting at $httpHost:$httpPort\nPress Return to stop...")
  StdIn.readLine()
  bindingFuture.onComplete(_=>actorSystem.terminate())
}
